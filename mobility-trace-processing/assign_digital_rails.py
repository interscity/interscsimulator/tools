import fileinput
import sys
import random

p = float(sys.argv[1])

for line in sys.stdin:
  result = line.replace('\n', '')
  if random.random() < p:
    result = result.replace('digital_rails_capable="false"', 'digital_rails_capable="true"')
    result = result.replace('name="', 'name="dr_')
  else:
    result = result.replace('name="', 'name="regular_')

  print(result)