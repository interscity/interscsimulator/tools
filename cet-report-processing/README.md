CET report processing 
===

At first we were intending to research how to process the [CET reports](http://www.cetsp.com.br/sobre-a-cet/relatorios-corporativos.aspx) automatically in order to obtain arterial ways in São Paulo fit to host Digital Rails. This turned to be not feasible in our timeframe due to the non-uniformity of the report formats. If you are looking for tools that help this proccess, [Tabula](https://tabula.technology/) is a good place to start. We switched to a manual analysis of the reports. This folder contains the results of this manual analysis.

## Region considered ##

![region](https://gitlab.com/interscity/interscsimulator/tools/raw/master/cet-report-processing/region.png "Digital Rails region")

## Methodology ##

We then selected the ways with the highest traffic volumes going downtown during the morning, as reported in the region in 2016 and 2017 reports.

In the [CET Volume Report, 2016](http://www.cetsp.com.br/media/574933/2016volumevelocidade.pdf), the selected routes were:

| Route ID | Name                                                                      | Traffic volume (peak 3 hours) |
|----------|---------------------------------------------------------------------------|-------------------------------|
| 10G      | Av. 23 de Maio, sob Vd. Beneficência Portuguesa                           | 25890                         |
| 31G      | Av. Pedro Álvares Cabral, entre Pça. Gal. Estilac Leal e R. Abílio Soares | 12183                         |


In the [CET Volume Report, 2017](http://www.cetsp.com.br/media/714822/msvp-2017-volume-e-velocidade.pdf), the selected routes were:

| Route ID | Name                                                                           | Traffic volume (peak hour) |
|----------|--------------------------------------------------------------------------------|----------------------------|
| 37G      | Av. Eng. Billings (EXPRESSA+LOCAL), sob Ponte da Cidade Universitária          | 12677                      |
| 14G      | Ligação Leste-Oeste, sobre a Av. 9 de Julho                                    | 5764                       |
| 12G      | Av. Cidade Jardim (Lateral + Túnel), entre R. Mário Ferraz e R. Franz Schubert | 4348                       |
| 13G      | Av. Paulista, entre R. Min. Rocha Azevedo e R. Padre João Manoel               | 3191                       |
| 01G      | Av. Eusébio Matoso, entre R. Cardeal Arcoverde e Av. das Nações Unidas         | 5320                       |

We then expanded the selected routes to aggregate the whole avenues or ways that they are on.

## Results ##
Our proposal is to host Digital Rails in the ways highlighted in green:

![proposal](https://gitlab.com/interscity/interscsimulator/tools/raw/master/cet-report-processing/proposal.png "Digital Rails proposal")

