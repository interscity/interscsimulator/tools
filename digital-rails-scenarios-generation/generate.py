import os

interscsimulator_root = '/home/gustavo/interscsimulator/'
scenarios_location = 'scenarios/digital-rails-av-paulista'

repetitions = 10
ratios = list(range(0, 101, 5))

if not os.path.exists(interscsimulator_root + scenarios_location):
  os.makedirs(interscsimulator_root + scenarios_location)

for ratio in ratios:
  scenario_name = 'peak_dr_algo_1_' + str(ratio)
  print('Generating {}. ratio = {}'.format(scenario_name, ratio / 100))

  scenario_path = scenarios_location + '/' + scenario_name
  if not os.path.exists(interscsimulator_root + scenario_path):
    os.makedirs(interscsimulator_root + scenario_path)

  for r in range(repetitions):
    os.system(
      "python ../trip-file-generation/generate_paulista.py {} > {}/trips{}.xml".format(
        ratio / 100, interscsimulator_root + scenario_path, r))

    config_template = """
<scsimulator_config>
  <config 
    trip_file="{scenario_path}/trips{r}.xml" 
    map_file="{scenarios_location}/network.xml" 
    output_file="{scenario_path}/events{r}.xml" 
    traffic_signals_file="{scenarios_location}/signals.xml"
    simulation_time="86400"/>
</scsimulator_config>
"""
    context = {"scenarios_location": scenarios_location, "scenario_path": scenario_path, "r": r} 

    with open('{}/config{}.xml'.format(interscsimulator_root + scenario_path, r),'w') as f:
      f.write(config_template.format(**context))
