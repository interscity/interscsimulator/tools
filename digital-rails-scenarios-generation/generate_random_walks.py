import os

interscsimulator_root = '/home/gustavo/interscsimulator/'
scenarios_location = 'scenarios/digital-rails-av-paulista'

ratios = list(range(0, 101, 5))

if not os.path.exists(interscsimulator_root + scenarios_location):
  os.makedirs(interscsimulator_root + scenarios_location)

for ratio in ratios:
  scenario_name = 'random_walk_dr_algo_1_' + str(ratio)
  print('Generating {}. ratio = {}'.format(scenario_name, ratio / 100))

  scenario_path = scenarios_location + '/' + scenario_name
  if not os.path.exists(interscsimulator_root + scenario_path):
    os.makedirs(interscsimulator_root + scenario_path)
    # 0.5 stands for 50% of OSM capacity:
    os.system("python ../trip-file-generation/generate_random_walks.py 0.5 {} > {}/trips.xml".format(ratio / 100, interscsimulator_root + scenario_path))

    config_template = """
<scsimulator_config>
  <config 
    trip_file="{scenario_path}/trips.xml" 
    map_file="{scenarios_location}/network.xml" 
    output_file="{scenario_path}/events.xml" 
    traffic_signals_file="{scenarios_location}/signals.xml"
    digital_rails_file="{scenarios_location}/digital-rails.xml"
    simulation_time="86400"/>
</scsimulator_config>
"""
    context = {"scenarios_location": scenarios_location, "scenario_path": scenario_path} 

    with open('{}/config.xml'.format(interscsimulator_root + scenario_path),'w') as f:
      f.write(config_template.format(**context))
