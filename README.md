interscsimulator-tools
===

Tools for processing inputs and outputs for [InterSCSimulator](https://github.com/ezambomsantana/smart_city_model).

## Prerequisites ##
Generate and activate a `venv`:

```sh
python3 -m venv venv                   
source venv/bin/activate
pip install -r requirements.txt
```

See directories above for detailed instructions for each task.
