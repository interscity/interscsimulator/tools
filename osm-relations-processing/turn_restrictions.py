import xml.etree.ElementTree as ET
import pandas as pd


def get_osm_restrictions(filename):
  osm_tree = ET.parse(filename)
  osm_root = osm_tree.getroot()

  restrictions = []
  osm_relations = osm_root.findall("relation")

  for r in osm_relations:
    restriction = {}
    relation_children = r.getchildren()
    for ch in relation_children:
      if ch.attrib.get("role") is not None:
        role = ch.attrib.get("role")
        if role == "from" or role == "to" or role == "via":
          restriction[role] = ch.attrib.get("ref")
      elif ch.attrib.get("k") is not None and ch.attrib.get("k") == "restriction":
        restriction["type"] = ch.attrib.get("v")
    if len(restriction.keys()) > 0:
      restrictions.append(restriction)

  return pd.DataFrame(restrictions)


def get_osm_ways(filename):
  osm_tree = ET.parse(filename)
  osm_root = osm_tree.getroot()

  ways = []
  osm_ways = osm_root.findall("way")

  for w in osm_ways:
    way = {
      'origid': w.attrib.get('id')
    }

    way_children = w.getchildren()
    for ch in way_children:
      if ch.attrib.get("k") is not None:
        if ch.attrib.get("k") == "name":
          way['name'] = ch.attrib.get("v")

    ways.append(way)

  return pd.DataFrame(ways)


def get_matsim_links(filename):
  matsim_tree = ET.parse(filename)
  matsim_root = matsim_tree.getroot()

  links = []
  xml_links = matsim_root.findall("links")[0].findall("link")

  for xml_link in xml_links:
    link = {
      'id': xml_link.attrib.get("id"),
      'from': xml_link.attrib.get("from"),
      'to': xml_link.attrib.get("to")
    }
    
    attributes = xml_link.findall("attributes")[0].findall("attribute")
    for attrib in attributes:
      if attrib.attrib.get("name") == "origid":
        link['origid'] = attrib.text

    links.append(link)

  return pd.DataFrame(links)


def get_link_restrictions(osm_filename, network_filename):
  wdf = get_osm_ways(osm_filename)

  rdf = get_osm_restrictions(osm_filename)
  rdf.columns = ['from_way', 'to_way', 'type', 'via_edge']

  ldf = get_matsim_links(network_filename)
  ldf.columns = ['from_edge', 'id', 'origid', 'to_edge']
  ldf = ldf.merge(wdf, on='origid')

  return ldf.merge(
    rdf, left_on=['to_edge', 'origid'], right_on=['via_edge', 'from_way']
  ).merge(
    ldf, left_on=['via_edge', 'to_way'], right_on=['from_edge', 'origid']
  )[['id_x', 'name_x', 'via_edge', 'type', 'id_y', 'name_y']]


def add_restricted_next_links():
  osm_filename = 'paulista.osm'
  network_filename = 'paulista.xml'

  links = get_matsim_links(network_filename)
  restrictions = get_link_restrictions(osm_filename, network_filename).set_index(['id_x', 'id_y'])[['type']].rename(columns={'type': 'restriction'})

  link_connections = links.merge(links, left_on='to', right_on='from', suffixes=['_from', '_to'])[['id_from', 'id_to']].set_index(['id_from', 'id_to'])
  link_connections['restriction'] = restrictions

  restrictions = restrictions.reset_index().set_index('id_x')
  link_connections = link_connections.reset_index().set_index('id_from')

  only_straight_on_complements = link_connections.loc[restrictions.loc[lambda df: df.restriction == 'only_straight_on'].index].copy().fillna('only_straight_on_complement').loc[lambda df: df.restriction != 'only_straight_on']
  others = link_connections.loc[lambda df: df.restriction != 'only_straight_on'].dropna()

  df = pd.concat([only_straight_on_complements, others])

  matsim_tree = ET.parse(network_filename)
  matsim_root = matsim_tree.getroot()

  xml_links = matsim_root.findall("links")[0].findall("link")
  xml_links_by_id = {}
  for xml_link in xml_links:
    link_id = xml_link.attrib.get('id')
    xml_links_by_id[link_id] = xml_link
  
  for link_id, restricted_next in df.drop_duplicates().groupby('id_from')['id_to'].apply(lambda s: ','.join(s)).iteritems():
    xml_links_by_id[link_id].set('restricted_next_links', restricted_next)

  matsim_tree.write(network_filename.replace('.xml', '.restricted.xml'))


if __name__ == '__main__':
  add_restricted_next_links()
