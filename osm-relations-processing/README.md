osm-relations-processing
===

Scripts to extract turn restrictions (forbidden left, right, u turns) from OpenStreetMap files (.osm) and translate them to the link ids used in MATsim generated networks.