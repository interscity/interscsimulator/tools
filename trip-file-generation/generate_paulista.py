import sys
import simtrip

# Script that generates trips for Av. Paulista
# TODO: Document better

cycle = 90

print("<scsimulator_matrix>")

dr_ratio = float(sys.argv[1])

paraiso_regular_cars = 2359 * (1-dr_ratio)
paraiso_dr_cars = 2359 * dr_ratio

consolacao_regular_cars = 3067 * (1-dr_ratio)
consolacao_dr_cars = 3067 * dr_ratio

paraiso_offset = cycle - 8 + 13  # Cycle time - time to get to first signalized intersection + start time
consolacao_offset = cycle - 5 - 58

# These link IDs can be generated differently for each osm -> matsim conversion.
# The values hardcoded here match the network at the master branch of the scenarios repository
paraiso_regular_trips = simtrip.generate_trips_uniform(
  name='paraiso_regular_', 
  origin='1952545091', 
  destination='60609874', 
  link_origin='5515', 
  cars_per_trip=1, 
  time_span_seconds=3600, 
  time_offset_seconds=paraiso_offset, 
  cars_per_hour=paraiso_regular_cars
)
for trip_xml in map(simtrip.trip_to_xml, paraiso_regular_trips):
  print(trip_xml)  

paraiso_dr_trips = simtrip.generate_trips_convoy(
  name='paraiso_dr_', 
  origin='1952545091', 
  destination='60609874', 
  link_origin='5515', 
  cycle_seconds=cycle,
  cars_per_trip=int(paraiso_dr_cars/(3600/cycle)), 
  time_span_seconds=3600, 
  time_offset_seconds=paraiso_offset, 
  cars_per_hour=paraiso_dr_cars
)
for trip_xml in map(simtrip.trip_to_xml, paraiso_dr_trips):
  print(trip_xml)  

consolacao_regular_trips = simtrip.generate_trips_uniform(
  name='consolacao_regular', 
  origin='60609819', 
  destination='1421376041', 
  link_origin='3347', 
  cars_per_trip=1, 
  time_span_seconds=3600, 
  time_offset_seconds=consolacao_offset, 
  cars_per_hour=consolacao_regular_cars
)
for trip_xml in map(simtrip.trip_to_xml, consolacao_regular_trips):
  print(trip_xml)  

consolacao_dr_trips = simtrip.generate_trips_convoy(
  name='consolacao_dr_', 
  origin='60609819', 
  destination='1421376041', 
  link_origin='3347',
  cycle_seconds=cycle, 
  cars_per_trip=int(consolacao_dr_cars/(3600/cycle)), 
  time_span_seconds=3600, 
  time_offset_seconds=consolacao_offset, 
  cars_per_hour=consolacao_dr_cars
)
for trip_xml in map(simtrip.trip_to_xml, consolacao_dr_trips):
  print(trip_xml)  

print("</scsimulator_matrix>")