import sys
import simtrip

osm_capacity_fraction = float(sys.argv[1])
digital_rails_capable_fraction = float(sys.argv[2])

# These link IDs can be generated differently for each osm -> matsim conversion.
# The values hardcoded here match the network at the master branch of the scenarios repository
trip_definitions = [
  {
    'name': 'paulista_paraiso_', 'origin': '60609819', 'destination': 'random_walk', 
    'link_origin': '5515', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*3000
  },
  {
    'name': 'paulista_consolacao_', 'origin': '1952545091', 'destination': 'random_walk', 
    'link_origin': '3347', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*3000
  },
  {
    'name': 'bela_cintra_', 'origin': '60624635', 'destination': 'random_walk', 
    'link_origin': '771', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
  {
    'name': 'haddock_lobo_', 'origin': '2006973647', 'destination': 'random_walk', 
    'link_origin': '2311', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'augusta_centro_', 'origin': '60624611', 'destination': 'random_walk', 
    'link_origin': '4358', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'augusta_bairro_', 'origin': '165467520', 'destination': 'random_walk', 
    'link_origin': '5896', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'rocha_azevedo_', 'origin': '60624571', 'destination': 'random_walk', 
    'link_origin': '3277', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'peixoto_gomide_', 'origin': '1735978065', 'destination': 'random_walk', 
    'link_origin': '4991', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'casa_branca_', 'origin': '60609766', 'destination': 'random_walk', 
    'link_origin': '1846', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'pamplona_', 'origin': '151275580', 'destination': 'random_walk', 
    'link_origin': '1511', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'campinas_', 'origin': '4259655877', 'destination': 'random_walk', 
    'link_origin': '2235', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'joaquim_eugenio_de_lima_', 'origin': '1990934596', 'destination': 'random_walk', 
    'link_origin': '2404', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'brig_luis_antonio_centro_', 'origin': '2397319383', 'destination': 'random_walk', 
    'link_origin': '2376', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'brig_luis_antonio_bairro_', 'origin': '142808169', 'destination': 'random_walk', 
    'link_origin': '4364', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'carlos_sampaio_', 'origin': '457593046', 'destination': 'random_walk', 
    'link_origin': '6896', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
  {
    'name': 'teixeira_da_silva_', 'origin': '303863292', 'destination': 'random_walk', 
    'link_origin': '1760', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'oswaldo_cruz_', 'origin': '4962324520', 'destination': 'random_walk', 
    'link_origin': '3878', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
]

print("<scsimulator_matrix>")

for definition in trip_definitions:
  regular_definition = dict(definition)
  dr_definition = dict(definition)

  regular_definition['name'] += 'regular_'
  regular_definition['cars_per_hour'] *= 1 - digital_rails_capable_fraction

  dr_definition['name'] += 'dr_'
  dr_definition['cars_per_hour'] *= digital_rails_capable_fraction

  trips = map(simtrip.trip_to_xml, simtrip.generate_trips_uniform(**regular_definition))
  for trip_xml in trips:
    print(trip_xml)

  trips = map(
    simtrip.trip_to_xml, 
    simtrip.generate_trips_uniform(digital_rails_capable='true', **dr_definition)
  )
  for trip_xml in trips:
    print(trip_xml)

print("</scsimulator_matrix>")