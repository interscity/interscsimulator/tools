import sys
import simtrip

osm_capacity_fraction = float(sys.argv[1])

# These link IDs can be generated differently for each osm -> matsim conversion.
# The values hardcoded here match the network at the master branch of the scenarios repository
trip_definitions = [
  {
    'name': 'bela_cintra_', 'origin': '60624635', 'destination': '165467521', 
    'link_origin': '771', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
  {
    'name': 'haddock_lobo_', 'origin': '2006973647', 'destination': '2006973650', 
    'link_origin': '2311', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'augusta_centro_', 'origin': '60624611', 'destination': '165467520', 
    'link_origin': '4358', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1000
  },
  {
    'name': 'augusta_bairro_', 'origin': '165467520', 'destination': '60624611', 
    'link_origin': '5896', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1000
  },
  {
    'name': 'rocha_azevedo_', 'origin': '60624571', 'destination': '165460240', 
    'link_origin': '3277', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'peixoto_gomide_', 'origin': '1735978065', 'destination': '60609797', 
    'link_origin': '4991', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'casa_branca_', 'origin': '60609766', 'destination': '165462605', 
    'link_origin': '1846', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'pamplona_', 'origin': '151275580', 'destination': '4259655867', 
    'link_origin': '1511', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'campinas_', 'origin': '4259655877', 'destination': '151275041', 
    'link_origin': '2235', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'joaquim_eugenio_de_lima_', 'origin': '1990934596', 'destination': '1990934599', 
    'link_origin': '2404', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'brig_luis_antonio_centro_', 'origin': '2397319383', 'destination': '142808169', 
    'link_origin': '2376', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'brig_luis_antonio_bairro_', 'origin': '142808169', 'destination': '142799353', 
    'link_origin': '4364', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1500
  },
  {
    'name': 'carlos_sampaio_', 'origin': '457593046', 'destination': '477229679', 
    'link_origin': '6896', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
  {
    'name': 'teixeira_da_silva_', 'origin': '303863292', 'destination': '303863648', 
    'link_origin': '1760', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*600
  },
  {
    'name': 'oswaldo_cruz_', 'origin': '4962324520', 'destination': '2466376871', 
    'link_origin': '3878', 'time_span_seconds': 3600, 'cars_per_hour': osm_capacity_fraction*1800
  },
]

print("<scsimulator_matrix>")

for definition in trip_definitions:
  trips = map(simtrip.trip_to_xml, simtrip.generate_trips_uniform(**definition))
  for trip_xml in trips:
    print(trip_xml)

print("</scsimulator_matrix>")